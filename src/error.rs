use std::io;
use std::num::ParseIntError;
use chrono::ParseError;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum SshAuthLogError {
    #[error("io error")]
    IOError(#[from] io::Error),

    #[error("integer parse error")]
    ParseIntError(#[from] ParseIntError),

    #[error("timestamp parse error")]
    ParseTimestampError(#[from] ParseError),

    #[error("unsupported format")]
    UnsupportedFormat
}
