use chrono::{Datelike, Local, NaiveDateTime};
use log::{error, info};
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::error::SshAuthLogError;
use crate::provider::AuthLogsProvider;

pub mod provider;
pub mod error;

#[derive(Serialize,Deserialize,PartialEq,Clone,Debug)]
pub struct KeyLoginAttempt {
    pub timestamp: NaiveDateTime,

    pub key_type: String,
    pub fingerprint_type: String,
    pub fingerprint: String,

    pub username: String,

    /// Offset number in `authorized_keys` file.
    ///
    /// Row part example:
    ///
    /// ```text
    /// ... /home/user/.ssh/authorized_keys:1
    /// ```
    pub key_offset: u32,
}

/// Looking for auth.log related files (auth.log.1, auth.log.1.gz, etc.) and extract login attempts with public key.
pub fn get_login_with_key_attempts(provider: &impl AuthLogsProvider) -> Result<Vec<KeyLoginAttempt>, SshAuthLogError> {
    let mut attempts: Vec<KeyLoginAttempt> = vec![];

    info!("get login with key attempts");

    let pattern = Regex::new("authorized_keys:\\d+$")
                                    .expect("unexpected error, invalid regular expression");

    let logs = provider.get_rows_contains(&pattern)?;

    let row_pattern = Regex::new("^(?P<timestamp>[a-zA-Z]+\\s\\d{1,2}\\s\\d{2}:\\d{2}:\\d{2}).*Accepted key\\s(?P<key_type>[a-zA-Z0-9]+)\\s(?P<fingerprint_type>[a-zA-Z0-9]+):(?P<fingerprint>[a-zA-Z0-9+/]+).*\\s(?P<filepath>[/a-zA-Z0-9._-]+):(?P<offset>\\d+)$")
        .expect("unexpected error, invalid regexp");

    let current_year = Local::now().year();

    for log_row in logs {
        for caps in row_pattern.captures_iter(&log_row) {
            let timestamp_str = format!("{}", &caps["timestamp"]);
            let key_type = format!("{}", &caps["key_type"]);
            let fingerprint_type = format!("{}", &caps["fingerprint_type"]);
            let fingerprint = format!("{}", &caps["fingerprint"]);

            let timestamp_with_year = format!("{timestamp_str} {current_year}");

            let timestamp = NaiveDateTime::parse_from_str(&timestamp_with_year, "%b %e %T %Y")?;

            let file_path_str = format!("{}", &caps["filepath"]);

            match get_username_from_home_path(&file_path_str) {
                Some(username) => {
                    let offset_str = format!("{}", &caps["offset"]);

                    let key_offset: u32 = offset_str.parse()?;

                    attempts.push(
                        KeyLoginAttempt {
                            timestamp,
                            key_type,
                            fingerprint_type,
                            fingerprint,
                            username,
                            key_offset,
                        }
                    )
                }
                None => error!("path '{file_path_str}' has unsupported format or doesn't contain username")
            }
        }
    }

    Ok(attempts)
}

/// Return username from home path
///
/// Supported path formats:
/// - `/home/username/something/else`
/// - `X:\Users\username\something\else`
fn get_username_from_home_path(path: &str) -> Option<String> {
    let lowercase_path = path.to_lowercase();

    let linux_path_pattern = Regex::new("^/home/(?P<username>[a-zA-Z0-9._-]+)/.*")
                                    .expect("unexpected error, invalid regular expression");

    if linux_path_pattern.is_match(&lowercase_path) {

        for caps in linux_path_pattern.captures_iter(&lowercase_path) {
            let username = format!("{}", &caps["username"]);
            return Some(username)
        }
    }

    None
}

#[cfg(test)]
mod get_username_from_home_path_tests {
    use crate::get_username_from_home_path;

    #[test]
    fn extract_username_from_absolute_paths() {
        let test_pairs = vec![
            ("/home/bob/.ssh/authorized_keys", "bob"),
            ("/home/rfeynman921_22/.ssh", "rfeynman921_22"),
            ("/home/johnny-3000/.ssh", "johnny-3000"),
        ];

        for (path, expected_username) in test_pairs {
            let username = get_username_from_home_path(&path).unwrap();
            assert_eq!(username, expected_username);
        }
    }

    #[test]
    fn return_none_for_relative_paths() {
        let paths = vec!["a/b/c", "\\Users\\user\\file", "some/relative/path/1.jpg", "abcdef", ""];

        for path in paths {
            assert!(get_username_from_home_path(&path).is_none())
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::{get_login_with_key_attempts, KeyLoginAttempt};
    use crate::provider::AuthLogFileProvider;

    const TESTS_DIR: &str = "test-data";

    #[test]
    fn attempts_should_be_read_from_current_auth_log_file() {
        let logs_path = Path::new(TESTS_DIR);

        let provider = AuthLogFileProvider::new(logs_path);

        let results = get_login_with_key_attempts(&provider).unwrap();

        assert_attempt(&results, "SNiDjsysmCYSk8fmtxtbHbMaQDDogv7P+IY6/mQKz9U", "proxy-user", 27);
        assert_attempt(&results, "SNiDjsysmCYSk8fmtxtbHbMaQDDogv7P+IY6/mQKz9U", "proxy-user", 13);
        assert_attempt(&results, "oCUpgneXmI2DtgLvkSGtzVEnrb0gE02N7pCNB3QJmB8", "rfeynman", 3);
        assert_attempt(&results, "oCUpgneXmI2DtgLvkSGtzVEnrb0gE02N7pCNB3QJmB8", "rfeynman", 1);
        assert_attempt(&results, "oCUpgneXmI2DtgLvkSGtzVEnrb0gE02N7pCNB3QJmB8", "tasya", 3);
        assert_attempt(&results, "oCUpgneXmI2DtgLvkSGtzVEnrb0gE02N7pCNB3QJmB8", "tasya", 5);
    }

    #[test]
    fn other_files_should_be_ignored() {
        let logs_path = Path::new(TESTS_DIR).join("empty");

        let provider = AuthLogFileProvider::new(logs_path.as_path());

        let results = get_login_with_key_attempts(&provider).unwrap();

        assert!(results.is_empty());
    }

    fn assert_attempt(attempts: &Vec<KeyLoginAttempt>, fingerprint: &str,
                      username: &str, key_offset: u32) {
        assert!(
            attempts.iter()
                .find(|a|
                    a.key_type == "RSA" && a.fingerprint_type == "SHA256" &&
                    a.fingerprint == fingerprint &&
                    a.username == username && a.key_offset == key_offset)
                .is_some()
        )
    }
}
