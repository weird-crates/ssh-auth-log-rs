use std::{fs, io};
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::path::{Path, PathBuf};
use regex::Regex;
use crate::error::SshAuthLogError;

pub trait AuthLogsProvider {
    fn get_rows_contains(&self, pattern: &Regex) -> Result<Vec<String>, SshAuthLogError>;
}

/// Reads and parse `auth.log` files.
///
/// Formats supported:
/// - `auth.log`
/// - `auth.log.1`
/// - `auth.log.N`
/// - `auth.log.7.gz`
pub struct AuthLogFileProvider {
    pub logs_path: PathBuf
}

impl AuthLogFileProvider {
    pub fn new(logs_path: &Path) -> AuthLogFileProvider {
        AuthLogFileProvider {
            logs_path: logs_path.to_path_buf(),
        }
    }

    fn get_rows_from_file(file_path: &Path, filter: &Regex) -> Result<Vec<String>, io::Error> {
        let mut lines: Vec<String> = vec![];

        let file_path_str = format!("{}", file_path.display());

        let file = File::open(file_path_str)?;
        let reader = BufReader::new(file);

        for line in reader.lines() {
            let row = line?;
            if filter.is_match(&row) {
                lines.push(row);
            }
        }

        Ok(lines)
    }

    fn get_rows_from_string(input: &str, filter: &Regex) -> Vec<String> {
        let mut lines: Vec<String> = vec![];

        for line in input.lines() {

            if filter.is_match(&line) {
                lines.push(line.to_string());
            }
        }

        lines
    }
}

impl AuthLogsProvider for AuthLogFileProvider {

    fn get_rows_contains(&self, pattern: &Regex) -> Result<Vec<String>, SshAuthLogError> {
        let mut lines: Vec<String> = vec![];

        let dir_entries = fs::read_dir(&self.logs_path)?;

        let file_name_pattern = Regex::new("^auth\\.log(\\.\\d+|\\.gz)?")
                                            .expect("invalid regular expression");

        for dir_entry in dir_entries.filter_map(|entry| entry.ok()) {

            match dir_entry.file_name().to_str() {
                Some(filename) => {
                    if file_name_pattern.is_match(filename) {

                        let path = dir_entry.path();

                        match path.extension() {
                            Some(extension) => {
                                match extension.to_str() {
                                    Some(file_ext) => {

                                        match file_ext {
                                            "gz" => {
                                                let mut buffer = String::new();
                                                autocompress::open(path)?.read_to_string(&mut buffer)?;

                                                let mut rows = AuthLogFileProvider::get_rows_from_string(&buffer, pattern);
                                                lines.append(&mut rows);
                                            }
                                            _ => {
                                                let mut rows = AuthLogFileProvider::get_rows_from_file(
                                                    &path.as_path(), pattern)?;
                                                lines.append(&mut rows);
                                            }
                                        }

                                    }
                                    None => {}
                                }
                            }
                            None => {}
                        }

                    }
                }
                None => {}
            }
        }

        Ok(lines)
    }

}

#[cfg(test)]
mod tests {
    use std::path::Path;
    use regex::Regex;
    use crate::provider::{AuthLogFileProvider, AuthLogsProvider};

    /// Expected files:
    /// - auth.log
    /// - auth.log.1
    /// - auth.log.N
    /// - auth.log.gz
    #[test]
    fn should_return_logs_only_in_special_log_files_and_ignore_other() {
        let path = Path::new("test-data");

        let provider = AuthLogFileProvider::new(path);

        let pattern = Regex::new("authorized_keys:\\d+$").unwrap();

        let rows = provider.get_rows_contains(&pattern).unwrap();

        let first_row = rows.first().unwrap();

        assert_eq!(
            "Mar 17 07:41:50 extl-ssh-proxy sshd[1011029]: Accepted key RSA SHA256:SNiDjsysmCYSk8fmtxtbHbMaQDDogv7P+IY6/mQKz9U found at /home/proxy-user/.ssh/authorized_keys:27",
            first_row
        );

        let last_row = rows.last().unwrap();

        assert_eq!(
            "Mar 14 11:27:55 extl-ssh-proxy sshd[1011034]: Accepted key RSA SHA256:oCUpgneXmI2DtgLvkSGtzVEnrb0gE02N7pCNB3QJmB8 found at /home/tasya/.ssh/authorized_keys:5",
            last_row
        );
    }

    #[test]
    fn empty_lines_should_be_ignored() {
        let path = Path::new("test-data");

        let provider = AuthLogFileProvider::new(path);

        let pattern = Regex::new("authorized_keys:\\d+$").unwrap();

        let rows = provider.get_rows_contains(&pattern).unwrap();

        assert_eq!(16, rows.len());
    }

    #[test]
    fn return_error_for_unknown_dir() {
        let path = Path::new("does-not-exist");
        let provider = AuthLogFileProvider::new(path);
        let pattern = Regex::new("authorized_keys:\\d+$").unwrap();
        assert!(provider.get_rows_contains(&pattern).is_err());
    }
}
