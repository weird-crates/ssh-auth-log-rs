# ssh auth log

## Get login attempts

Function looking for auth.log related files (auth.log.1, auth.log.1.gz, etc.) and extract login attempts with public key.

```rust
let logs_path = Path::new("/var/log");
let provider = AuthLogFileProvider::new(logs_path);

let attempts = get_login_with_key_attempts(&provider)?;
```

Attempt entity `KeyLoginAttempt`.

## Limitations

- We mean that all dates from logs from current year. I didn't beat a problem yet with chrono date parsing.
